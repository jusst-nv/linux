/*
 *  Board-specific setup code for the AT91SAM9M10G45 Evaluation Kit family
 *
 *  Covers: * AT91SAM9G45-EKES  board
 *          * AT91SAM9M10G45-EK board
 *
 *  Copyright (C) 2009 Atmel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 */

#include <linux/types.h>
#include <linux/gpio.h>
#include <linux/init.h>
#include <linux/mm.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/spi/spi.h>
#include <linux/fb.h>
#include <linux/gpio_keys.h>
#include <linux/input.h>
#include <linux/leds.h>
#include <linux/atmel-mci.h>
#include <linux/delay.h>
#include <linux/netdevice.h>
#include <linux/phy.h>
#include <linux/interrupt.h>
#include <linux/input.h>
#include <net/dsa.h>

#include <linux/platform_data/at91_adc.h>

#include <mach/hardware.h>
#include <video/atmel_lcdc.h>
#include <media/soc_camera.h>
#include <media/atmel-isi.h>

#include <asm/setup.h>
#include <asm/mach-types.h>
#include <asm/irq.h>

#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach/irq.h>

#include <mach/at91sam9_smc.h>
#include <mach/system_rev.h>

#include "at91_aic.h"
#include "at91_shdwc.h"
#include "board.h"
#include "sam9_smc.h"
#include "generic.h"
#include "gpio.h"

#include <linux/clk.h>
#include "clock.h"

#define SCKCR (0xd50)

static void set_alarm(void)
{
	void __iomem *r = ioremap(0xfffff000, 0x1000);

	if (!r)
		return;
	writel(0x02, r + 0xdcc); /* reset ALARM clock state */
	iounmap(r);
}

static void set_slowclock(void)
{
	void __iomem *r = ioremap(0xfffff000, 0x1000);

	if (!r)
		return;
	//writel(0x02, r + 0xdcc); /* reset ALARM clock state */
        if (readl(r + SCKCR) == 1) {
                writel(0x03, r + SCKCR);
                mdelay(1400);
                writel(0x0b, r + SCKCR);
                mdelay(1);
                writel(0x0a, r + SCKCR);
        }
	iounmap(r);
}


static void __init octonet_init_early(void)
{
	/* Initialize processor: 12.000 MHz crystal */
	at91_initialize(12000000);

	set_alarm();
}

/*
 * USB HS Host port (common to OHCI & EHCI)
 */
static struct at91_usbh_data __initdata ek_usbh_hs_data = {
	.ports		= 2,
	.vbus_pin	= {-1, -1},
	.vbus_pin_active_low = {1, 1},
	.overcurrent_pin= {-EINVAL, -EINVAL},
};


/*
 * USB HS Device port
 */
static struct usba_platform_data __initdata ek_usba_udc_data = {
	.vbus_pin	= AT91_PIN_PB19,
};

/*
 * MACB Ethernet device
 */
static struct macb_platform_data __initdata ek_macb_data = {
	.phy_irq_pin	= 0,
	.is_rmii	= 0,
};

/*
 * NAND flash
 */
static struct mtd_partition __initdata ek_nand_partition[] = {
	{
		.name	= "boot",
		.offset	= 0,
		.size	= 0x100000,
	},
	{
		.name	= "linux",
		.offset	= MTDPART_OFS_NXTBLK,
		.size	= 0x1f00000,
	},
	{
		.name	= "ubi",
		.offset	= MTDPART_OFS_NXTBLK,
		.size	= MTDPART_SIZ_FULL,
	},
};

static struct atmel_nand_data __initdata ek_nand_data = {
	.ale		= 21,
	.cle		= 22,
	.rdy_pin	= AT91_PIN_PC8,
	.enable_pin	= AT91_PIN_PC14,
	.det_pin	= -EINVAL,
	.ecc_mode	= NAND_ECC_SOFT_BCH,
	.ecc_size	= 512,
	.ecc_bytes	= 13,
	.on_flash_bbt	= 1,
	.parts		= ek_nand_partition,
	.num_parts	= ARRAY_SIZE(ek_nand_partition),
	.bus_width_16	= 0,
};

static struct sam9_smc_config __initdata ek_nand_smc_config = {
	.ncs_read_setup		= 0,
	.nrd_setup		= 2,
	.ncs_write_setup	= 0,
	.nwe_setup		= 2,

	.ncs_read_pulse		= 4,
	.nrd_pulse		= 4,
	.ncs_write_pulse	= 4,
	.nwe_pulse		= 4,

	.read_cycle		= 7,
	.write_cycle		= 7,

	.mode			= AT91_SMC_READMODE | AT91_SMC_WRITEMODE | AT91_SMC_EXNWMODE_DISABLE,
	.tdf_cycles		= 3,
};

static void __init ek_add_device_nand(void)
{
	/* setup bus-width (8 or 16) */
	if (ek_nand_data.bus_width_16)
		ek_nand_smc_config.mode |= AT91_SMC_DBW_16;
	else
		ek_nand_smc_config.mode |= AT91_SMC_DBW_8;

	/* configure chip-select 3 (NAND) */
	sam9_smc_configure(0, 3, &ek_nand_smc_config);

	at91_add_device_nand(&ek_nand_data);
}

static struct resource octonet_dvb_resources[2] = { 
        [0] = { 
                .start  = 0x30000000, 
                .end    = 0x30000000 + SZ_64K - 1, 
                .flags  = IORESOURCE_MEM, 
        }, 
        [1] = { 
                .start  = 0,
                .end    = 0,
                .flags  = IORESOURCE_IRQ, 
        }, 
}; 

static void octonet_dvb_release(struct device *dev)
{

}

static struct platform_device octonet_dvb_device = { 
        .name           = "octonet-dvb", 
        .id             = 0, 
        .dev            = { 
		.release           = octonet_dvb_release,
        }, 
        .resource       = octonet_dvb_resources, 
        .num_resources  = ARRAY_SIZE(octonet_dvb_resources), 
}; 

static void __init add_octonet_dvb(void)
{
	u32 pwr_gpio = AT91_PIN_PD11;
	u32 gpio = AT91_PIN_PC15;

	at91_set_gpio_output(pwr_gpio, 1);

	at91_set_GPIO_periph(gpio, 0);
	at91_set_gpio_input(gpio, 0);
	at91_set_deglitch(gpio, 0);
	octonet_dvb_device.resource[1].start = gpio_to_irq(gpio);
	octonet_dvb_device.resource[1].end = gpio_to_irq(gpio);
	platform_device_register(&octonet_dvb_device);
}


static struct gpio_keys_button octonet_buttons[] = {
	{
		.code		= KEY_POWER,
		.gpio		= AT91_PIN_PC12,
		.active_low	= 1,
		.desc		= "power",
		.wakeup		= 1,
		.type           = EV_KEY,
	},
};

static struct gpio_keys_platform_data octonet_button_data = {
	.buttons	= octonet_buttons,
	.nbuttons	= ARRAY_SIZE(octonet_buttons),
};

static struct platform_device octonet_button_device = {
	.name		= "gpio-keys",
	.id		= -1,
	.num_resources	= 0,
	.dev		= {
		.platform_data	= &octonet_button_data,
	}
};

static void __init octonet_add_buttons(void)
{
	int i;
	
	for (i = 0; i < ARRAY_SIZE(octonet_buttons); i++) {
		at91_set_GPIO_periph(octonet_buttons[i].gpio, 1);
		at91_set_deglitch(octonet_buttons[i].gpio, 1);
	}
	platform_device_register(&octonet_button_device);
}


static struct input_dev *pwr;
static struct timer_list pwr_timer;

static void pwr_timer_func(unsigned long ptr)
{
	input_event(pwr, EV_KEY, KEY_POWER, 1);
	input_event(pwr, EV_KEY, KEY_POWER, 0);
	input_sync(pwr);
}


#if 0
static irqreturn_t pwr_irq(int irq, void *dev_id) 
{
	u32 val = gpio_get_value(AT91_PIN_PC12);
	static int sw = 0;

	if (val & 1) {
		if (del_timer(&pwr_timer)) {
			sw ^= 1;
			input_event(pwr, EV_KEY, KEY_PAUSE, sw);
			input_sync(pwr);
		}
	} else {
		mod_timer(&pwr_timer, jiffies + 3 * HZ);
	}
	//input_event(pwr, EV_KEY, KEY_POWER, val ^ 1);
	//input_sync(pwr);
	return IRQ_HANDLED;
}
#else
static irqreturn_t pwr_irq(int irq, void *dev_id) 
{
	u32 val = gpio_get_value(AT91_PIN_PC12);
	static int sw = 0;

	input_event(pwr, EV_KEY, KEY_POWER, val ^ 1);
	input_sync(pwr);
	return IRQ_HANDLED;
}
#endif

static void __init add_octonet_button(void)
{
	u32 gpio = AT91_PIN_PC12;
	u32 irq;

	at91_set_GPIO_periph(gpio, 0);
	at91_set_gpio_input(gpio, 0);
	at91_set_deglitch(gpio, 0);

	init_timer(&pwr_timer);
	pwr_timer.function = pwr_timer_func;
	pwr_timer.data = 0;
	pwr = input_allocate_device();
	if (!pwr) {
		printk("could not allocate input device\n");
		return;
	}

	set_bit(EV_KEY, pwr->evbit);
	set_bit(KEY_POWER, pwr->keybit);
	set_bit(KEY_PAUSE, pwr->keybit);
	pwr->name = "octonet_pwr";
	pwr->phys = "octonet_pwr/input0";

	irq = gpio_to_irq(gpio);
	request_irq(irq, pwr_irq, IRQF_TRIGGER_RISING | IRQF_TRIGGER_FALLING, "pwr", NULL);

	input_register_device(pwr);
}

static struct dsa_chip_data switch_chip_data = {
	.port_names[0]	= "lan1",
	.port_names[1]	= "lan2",
	.port_names[2]	= "lan3",
	.port_names[3]	= "lan4",
	.port_names[4]	= "lan5",
	.port_names[5]	= "cpu",
	.port_names[6]	= "fpga",
};

static struct dsa_platform_data switch_data = {
	.nr_chips	= 1,
	.chip		= &switch_chip_data,
};

static struct resource switch_resources[] = {
	{
		.start	= 0,
		.end	= 0,
		.flags	= IORESOURCE_IRQ,
	},
};

static struct platform_device switch_device = {
	.name		= "dsa",
	.id		= 0,
	.num_resources	= 0,
	.resource	= switch_resources,
	.dev.platform_data  = &switch_data,
};

extern struct platform_device at91sam9g45_eth_device;

static void __init add_switch(void)
{
	int i;
	struct platform_device *pdev = &at91sam9g45_eth_device;
	struct net_device *netdev = platform_get_drvdata(pdev);
	struct mii_bus *mii = (struct mii_bus *) dev_get_drvdata(&netdev->dev);

//	printk("%08x %08x %08x\n", (u32) pdev, (u32) netdev, (u32) &netdev->dev);
	switch_data.netdev = &netdev->dev;
	for (i=0; i<5; i++)
		switch_data.chip[i].mii_bus = &mii->dev;
	platform_device_register(&switch_device);
}



static void set_pck(void)
{
	struct clk *pck0, *mck;
	int res;

	pck0 = clk_get(NULL, "pck0");

	clk_enable(pck0);
	return;


	mck = clk_get(NULL, "mck");
	at91_set_A_periph(AT91_PIN_PD26, 0);

	printk("pck=%08x %d,  mck=%08x %d\n", 
	       (u32) pck0, pck0->type, (u32) mck, mck->type);

	res = clk_set_parent(pck0, mck);
	printk("set_parent = %d\n", res);
	//clk_put(mck);

}

#include <mach/at91sam9g45_matrix.h>
#include <mach/at91_matrix.h>

#undef AT91_SCKCR 
#define AT91_SCKCR (void __iomem *)(0xfffffd50)

static void octonet_init(void)
{
	at91_matrix_write(AT91_MATRIX_EBICSA, 0x0001000a);

	/* DGBU on ttyS0. (Rx & Tx only) */
	at91_register_uart(0, 0, 0);
	at91_add_device_serial();

	/* USB HS Host */
	at91_add_device_usbh_ohci(&ek_usbh_hs_data);
	at91_add_device_usbh_ehci(&ek_usbh_hs_data);
	/* USB HS Device */
	//at91_add_device_usba(&ek_usba_udc_data);
	/* Ethernet */
	at91_add_device_eth(&ek_macb_data);

	at91_set_gpio_input(AT91_PIN_PA19, 0);	
	at91_set_gpio_input(AT91_PIN_PA18, 0);

	at91_matrix_write(AT91_MATRIX_SCFG4, (8<<18)|(2<<16));
	at91_matrix_write(AT91_MATRIX_PRBS4, 0x00000003);
	//at91_matrix_write(AT91_MATRIX_SCFG5, (8<<18)|(2<<16));
	//at91_matrix_write(AT91_MATRIX_PRBS5, 0x00000003);
	//at91_matrix_write(AT91_MATRIX_VIDEO, AT91C_VDEC_SEL_OFF);
	ek_add_device_nand();
	//at91_add_device_i2c(0, NULL, 0);
	add_octonet_dvb();

	set_pck();

	at91_shdwc_write(AT91_SHDW_MR, 0x30003);
	//octonet_add_buttons();
	set_slowclock();
}

static void octonet_init_late(void)
{
	add_switch();
	add_octonet_button();
}

MACHINE_START(NAXY400, "DigitalDevices OctopusNet")
	.init_time	= at91sam926x_pit_init,
	.map_io		= at91_map_io,
	.handle_irq	= at91_aic_handle_irq,
	.init_early	= octonet_init_early,
	.init_irq	= at91_init_irq_default,
	.init_machine	= octonet_init,
	.init_late	= octonet_init_late,
MACHINE_END
